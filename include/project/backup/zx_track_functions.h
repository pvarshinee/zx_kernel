/*
 * zx_track_functions.h
 *
 * Authors : Alex Murashkin
 *           Priyaa Varshinee
 */
 
#ifndef ZX_TRACK_FUNCTIONS_H
  
  #define ZX_TRACK_FUNCTIONS_H
  
   #include <zx_track_sensing.h>
  
   void track_init(char track_id, track_node* track);
   void turnout_commit(zx_turnout_state* turnout);
   void turnout_toggle(zx_turnout_state turnout_states[], int id, char cstate);
   void init_tracka( track_node* );
   void init_trackb( track_node* );
   
   void zx_track_sensor_task();  
   void zx_sensor_server_task();  
    
#endif /* TRACK_FUNCTIONS_H */
