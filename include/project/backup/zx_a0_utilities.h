/*
 * zx_a0_utilities.h
 *
 * Authors : Alex Murashkin
 *           Priyaa Varshinee
 */
 
 #ifndef ZX_A0_UTILITIES
 
 #define ZX_A0_UTILITIES
 
   #define FOREVER for(;;)
 
   void num_to_string( unsigned int num, unsigned int base, char *bf );
   int process_command( char *buffer, int *arg1, int *arg2, int *arg3, int *arg4);
   
   int strcmp( char *str1, char *str2 );
  
 #endif /* ZX_A0_UTILITIES */
