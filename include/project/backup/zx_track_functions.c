/*
 * zx_track_functions.c
 *
 * Authors : Alex Murashkin
 *           Priyaa Varshinee
 */
 
 #include <zx_track_functions.h>
 
void track_init(char track_id, track_node* track)
{
   if (track_id == 'A')
      init_tracka(track);

   else if (track_id == 'B')
      init_trackb(track);
      
}

void turnout_commit(zx_turnout_state* turnout)
{
   char com1_output[50];
   int com1_output_size = 0;
   int switch_value;

   if (turnout->state == TURNOUT_STATE_CURVED)
   {
      switch_value = 34;
   }
   else
   {
      switch_value = 33;      
   } 
   
   com1_output_size = 0;
      com1_output[com1_output_size++] = switch_value;
      com1_output[com1_output_size++] = turnout->id;
   com1_output[com1_output_size++] = UART_PACKET_END_CHAR;
   PutMultiple( COM1, com1_output );                 

}

void turnout_toggle(zx_turnout_state turnout_states[], int id, char cstate)
{
   
   if (cstate == 's' || cstate == 'S')
   {
      turnout_states[id].state = TURNOUT_STATE_STRAIGHT;
   }
   else 
   {
      turnout_states[id].state = TURNOUT_STATE_CURVED;      
   }
      
   turnout_commit(&turnout_states[id]);   
}

void turnout_init(zx_turnout_state turnout_states[], char cstate) // 's' or 'c'
{
   int i;
   int state;
   
   if (cstate == 's' || cstate == 'S')
   {
      state = TURNOUT_STATE_STRAIGHT;
   }
   else 
   {
      state = TURNOUT_STATE_CURVED;      
   }
   
   for (i = 0; i < TURNOUT_BUF_SIZE; i++)
   {
      if ((i >= 1 && i <= 22) || (i >= 153))
      {
         turnout_states[i].id = i;
         turnout_states[i].state = state;
         turnout_states[i].time = 0;                  
      } 
      else
      {
         turnout_states[i].id = 0; // just ignore this turnout      
      }
   }

   for (i = 0; i < TURNOUT_BUF_SIZE; i++)
   {
      if (turnout_states[i].id != 0)
      {
         turnout_commit(&turnout_states[i]);   
      }
   }   
      
} 

