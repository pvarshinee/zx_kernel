/*
 * zx_track_sensing.h
 *
 * Authors : Alex Murashkin
 *           Priyaa Varshinee
 */

#ifndef TRACK_SENSING_H

   #define TRACK_SENSING_H

   #define FOREVER for(;;)

   #include "zx_track_data.h"
   
   typedef struct {
//      int node_id; 
      int time; // time at which the train hit the node
                // in case of expected sesnors, it expected to be expected time
      track_node* node;
      int dist; // distance of the train from any last node in case of last node info
                // in case of expected sensors/nodes, the distance to the sensors/nodes from the train's current position
     
   } zx_node_info;
   
   #define DISTANCE_UNIT 100
   
   
   typedef struct {
//      int node_id; 
      int time; // time at which the train hit the node
                // in case of expected sesnors, it expected to be expected time
      track_node* node;
      int dist; // distance of the train from any last node in case of last node info
                // in case of expected sensors/nodes, the distance to the sensors/nodes from the train's current position
      track_edge* edge;
    
   } zx_train_pos;

      
   typedef struct {
      track_node* node;
      int distance_to_node;
      int offset_from_node; 
      int visible;    
   
   } zx_train_target;

   
   #define TRAIN_HT_LENGTH 120
   
   #define NEXT_SENSOR_COUNT 15
   #define NEXT_NODE_COUNT 5
   
   // just one recursion step down
   #define NEXT_SENSOR_LOOKUP_DEPTH 1

   // look for three sensor on a non-branched parts
   #define NEXT_SENSOR_LOOKUP_BREADTH 3



   typedef struct {
      int node_id;
      int offset;
      int distance; // distance to the waypoint
   } zx_waypoint;

   #define WAYPOINT_COUNT 5

   typedef struct {
      int state;
      
   } zx_train_route;

   
   typedef struct{
      int train_id; // donezx_train_stop_request

      zx_train_pos head_pos; // done --
      zx_train_pos tail_pos;
      
      int distance_travelled_in_delta;

      int velocity_state; // acclerating, deccelerating, immobile
       
      int initial_velocity; // v0
      int final_velocity;   // v1
      int current_velocity; // the current velocity we calculate at each step becomes initial velocity for next step
      
      int t1;
      int t0;
      int t;
      
      int speed; // to know if we are accelerating or deccelerating
      
      /*
         stopping time, t1 should be calculated dynamically with stopping distance and velocity. Here Stopping distance
         is constant but velocity is not.
         !! IMPORTANT : These distances assosciate to accelerated speeds. The velocity varies for the same speed during 
            acceleration and decceleration
      */
      int stopping_distance[15]; // static list of stoping distances for this train. One distance for each speed. Speed starts from 0,1,2... ) , 0 means stop
     
      /* we store the velocity associated with each speed. this will vary for some trains
         depending on it accelerated to deccelerated. 
         So  0 is for acceleration velocities
         and 1 is for decceleration velocities
       */
      int speed_to_velocity[2][15];   // Speed starts from 0,1,2... ) 0 means stop                                      

      zx_node_info expected_sensors[NEXT_SENSOR_COUNT];
//      zx_node_info expected_nodes[NEXT_NODE_COUNT];
      
      int direction; // 1 for ahead, -1 for reverse
      
      int last_sensor_id;
      
      zx_train_target target;

      zx_waypoint waypoints[WAYPOINT_COUNT];
      int waypoint_index;
      
      int route [TRACK_MAX];
      int route_node_index;

   } zx_train;




   #define TRAIN_COUNT 1

   typedef struct {
      zx_train *train;
      int expected_time;
   }zx_train_stop_request;

   typedef struct {
      int value;
      int time;
   } zx_sensor_request;



   typedef struct {
      int value;
   } zx_ui_request;


   typedef struct {
      int id;
      int state;
      int time; // nto sure
   } zx_turnout_state;
   
   typedef struct {
      int type;
      int source_id;
      int destination_id;   
      track_node* track_ptr;
      zx_turnout_state* turnout_states;
      int print_row;
      zx_train* train;
      int include_reverse;
      
   } zx_route_request;
   
   
   #define ROUTE_REQUEST_SHORTEST 1
   #define ROUTE_REQUEST_REVERSES 2
   #define ROUTE_REQUEST_EXIT 3   


      
   typedef struct {
      int type;
   } zx_route_response;
   

   void zx_train_controller_task();
   
   //test functions
   void train_kernel_stress_testing();
     
   void zx_route_task();
 
   
   #define TRACK_STATE_INIT 0
   #define TRACK_STATE_RUN 1
   #define TRACK_STATE_UI_WAIT 2
   #define TRACK_STATE_STOP_WAIT 3
   #define TRACK_STATE_RUN_FIRST 4
   
   #define SENSOR_BUF_SIZE 10 * sizeof(zx_sensor_request)

   
   #define TURNOUT_STATE_STRAIGHT 0
   #define TURNOUT_STATE_CURVED 1
   
   #define TURNOUT_BUF_SIZE 157 

   
   #define TRAIN_STATE_IMMOBILE 0      
   #define TRAIN_STATE_ACCELERATING 1
   #define TRAIN_STATE_UNIFORM 2   
   #define TRAIN_STATE_DECCELERATING 3
   

#endif
