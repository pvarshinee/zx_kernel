/*
 * zx_shortest_path.h
 *
 * Authors : Alex Murashkin
 *           Priyaa Varshinee
 */

#include "zx_track_sensing.h"

#define ZX_INFINITY 0x0fffffff // sufficiently large
#define YES 'y'
#define NO  'n'

int find_shortest_distance_node( int distance[], char visited[]);
int zx_shortest_path( int source_index, int target_index, int path_stack[], int distance[], int *path_stack_top, track_node track[]);
void zx_prepare_track( zx_turnout_state turnout_states[], int path_stack[], track_node track[], int path_stack_top);

int track_calc_shortest_path(track_node* track, int source_index, int destination_index, zx_turnout_state turnout_states[], char* result_path, int* result_path_length, zx_waypoint waypoints[]);
void zx_get_waypoints(  zx_waypoint waypoints[], int path_stack[], int path_distance[], track_node track[], int path_stack_top);

// reverse offset (mm)
#define reverse_offset 200
