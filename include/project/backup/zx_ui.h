/*
 * zx_ui.h
 *
 * Authors : Alex Murashkin
 *           Priyaa Varshinee
 */
 
 /* This file contains functions to manage the 
  * screen output while the trains are running
  */
 
#ifndef ZX_UI_H
#define ZX_UI_H
   
  #include <zx_track_sensing.h>

  typedef struct{
   int arg1;
   int arg2;
   int arg3;
   int arg4;
  } zx_command_task_request;
  
  void print_train_data(track_node* track, zx_train* train, int idle_task_percent );
  void print_initial_screen();
  void zx_ui_input_task();
  void zx_ui_server_task();
  
#endif /* ZX_UI */
